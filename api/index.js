const express = require('express')
const rateLimit = require('express-rate-limit')

express()
  .use(rateLimit({ windowMs: 1000, max: 5 }))
  .use(require('body-parser').json())
  .post('/', (req, res) => res.send(req.body))
  .listen(3000)
