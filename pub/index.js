const rabbit = require('amqplib')

;(async () => {
  try {
    const connection = await rabbit.connect('amqp://rabbit')
    const channel = await connection.createChannel()
    await channel.assertQueue('queue', { durable: true })

    const sendInterval = setInterval(
      () => channel.sendToQueue(
        'queue',
        Buffer.from(JSON.stringify({some: 'payload'}))
      ),
      100
    )

    setTimeout(
      () => {
        clearInterval(sendInterval)
        setTimeout(
          () => connection.close(),
          1000
        )
      },
      10000
    )
  } catch (e) {
    process.exit(1)
  }
})()
