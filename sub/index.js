const axios = require('axios')
const rabbit = require('amqplib')

const waitTilTime = (time) =>
  new Promise((resolve) => {
    const waitTime = time - new Date().getTime()

    setTimeout(() => {
      return resolve()
    }, waitTime)
  })


const getRndInteger = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min

// Using same algorithm as KafkaJS retry mechanism: https://kafka.js.org/docs/retry-detailed
const getNewRetryTime = (previousRetryTime, factor = 0.2, multiplier = 1.5) =>
  getRndInteger(previousRetryTime * (1 - factor), previousRetryTime * (1 + factor)) * multiplier

const processMsg = (channel, retryWaitTime) => async (msg) => {
  const msgParsed = JSON.parse(msg.content.toString())

  try {
    const response = await axios.post('http://api:3000', msgParsed)

    const remainingRequests = response.headers['x-ratelimit-remaining']

    // If remainingRequests is not available we will default to retryWaitTime when a 429 error happens
    if (remainingRequests < 1) { // We ran out of requests

      // Try to look for the reset time and if it is available use it to know when the next request can 
      // be made. Otherwise use the default retry wait time `retryWaitTime`
      const resetRateLimit = response.headers['x-ratelimit-reset']
      await waitTilTime((resetRateLimit && resetRateLimit * 1000) || new Date().getTime() + retryWaitTime)
    }

    return channel.ack(msg)
  } catch (error) {
    console.log(`Failed to process msg: ${msgParsed.some}`)

    // If we get a rate limit error from the API we try to reprocess the message.
    // The time to wait for making the next API request is calculated using randomised mechanism that will increase the 
    // returned value in each retrial, to avoid two things:
    // 1. Debounce the time we send the request to the API, so in each iteration we wait a bit more 
    // 2. Add a jitter between all concurrent clients, so not all of them send the request at the same time in each iteration
    if (error.response && error.response.status === 429) {
      console.log(`${error.response.status} - ${error.response.data}`)

      const newRetryTime = getNewRetryTime(retryWaitTime)

      console.log('new retry time', newRetryTime);

      setTimeout(() => {
        return processMsg(channel, newRetryTime)(msg)
      }, retryWaitTime)
    } else if (error.request) {
      console.log(error.request)
    } else {
      console.log('Error', error.message)
    }
  }
}

const run = async () => {
  try {
    const connection = await rabbit.connect('amqp://rabbit')
    const channel = await connection.createChannel()
    await channel.assertQueue('queue', { durable: true })

    console.log('subscribed')

    channel.prefetch(1)

    const INITIAL_RETRY_TIME = 300

    return channel.consume('queue', processMsg(channel, INITIAL_RETRY_TIME))
  } catch (e) {
    console.log(e)
    process.exit(1)
  }
}

(async () => await run())()
