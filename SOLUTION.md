# Solution brainstorm

### Fix in API project

First of all, I had to fix an issue in the API project due to the fact that the way body-parser was instantiated was deprecated, so the API was not able to process any request, only to return rate limited errors: 

```diff
const express = require('express')
const rateLimit = require('express-rate-limit')

express()
  .use(rateLimit({ windowMs: 1000, max: 5 }))
-  .use(require('body-parser'))
+  .use(require('body-parser').json())
  .post('/', (req, res) => res.send(req.body))
  .listen(3000)
```

# Solution walk-around

**Note**: I'll be using `axios` instead of `http` to simplify the code.

There are different options to fix the issue without actually modifing the producer nor the API. 

## Brute force

The first solution I came up with is a brute force algorithm, basically what it tries to do is forcing the messages to go through the API, if we get back an status code 429 we throw the message back to the queue using `nack` until the throtling in the API frees up for more messages. 

```javascript
    if (error.response.status === 429) {
      console.log(`${error.response.status} - ${error.response.data}`);

      channel.nack(msg);
    }
```

This is a pretty bad solution, as we're forcing the API and wasting a lot of resources, but gives me a good starting point for improvement.

## Reliable solution

Second option is to limit the quantity of messages a subscriber can read concurrently to 1: `channel.prefetch(1)`.

Then, if we get a throttling status code (`429`), instead of putting the message back to the queue, we can try to send it again to the API, waiting a grace period to avoid the throttling: 

```js
    if (error.response && error.response.status === 429) {
      console.log(`${error.response.status} - ${error.response.data}`);

      setTimeout(() => {
        return processMsg(channel, retryWaitTime)(msg); // Recursive call to the same function
      }, retryWaitTime)
    }
```

In the situation where we have **100** messages to process, and a window of 1000ms which we can only send a maximum of 5 messages each:  

The fastest time in which we can process all the messages **with a single subscriber** is around ~20 seconds. This is due to the fact that for each batch of 5 messages (if sent in parallel) we have to wait around ~1 second to be able to send more messages. Therefore, *100 msg / 5 msg per batch = 20 batches*, with a wait time of 1 second between them makes a total time of around ~20 seconds.

Even though this solution is already working without losing any message, it is pretty conservative, as we're kind of guessing the reset window of time of our API using a predefined waiting time when we receive a `429` error. Furthermore, we are still forcing the API to work when we're overpassing the limit. Which in a real-world scenario it can even force a permanent ban from the API owners. 

## Looking at the Headers

A way of avoiding this, is as simple as looking at the **Headers** in the response. In those we can find the remaining allowed calls we have, and how much time do we have to wait until the rate limit resets. Using those we can develop a simple algorithm so whenever the limit is overpassed, we wait the remaining time for the reset event.


```js
    const waitTilTime = (time) =>
      new Promise((resolve) => {
        const waitTime = time - new Date().getTime();

        setTimeout(() => {
          return resolve();
        }, waitTime)
      });

    const remainingRequestsRateLimit = response.headers['x-ratelimit-remaining'];

    if (remainingRequestsRateLimit < 1) {
      const resetRateLimit = response.headers['x-ratelimit-reset'];
      await waitTilTime((resetRateLimit && resetRateLimit * 1000) || new Date().getTime() + retryWaitTime);
    }
```

Using this as a circuit breaker, in the last request sent we can decide if proceed with the flow or wait the specific period provided by the API, avoiding wasting requests, improving resource optimisation and the time it takes to process all the messages, as this will be more precise than the previous solution. 

Combining both methods is the most reliable solution as if the headers are present we would use those, but if there aren't we can rely on the retry solution waiting a grace period.

### Scaling up

In both scenarios scaling up the number of subscriber instances will linearly decrease the processing time. We rely on RabbitMQ to send each subscriber a different message. 

All of this applies if the IP of each subscriber is different, if this does not apply then this solution is still being reliable but we cannot really avoid the bottleneck at the API level and get much better results. Even if we increment the number of subscribers we will be able to process at maximum 5 requests per second, which is the rate limit we have at the API level. 


### Retry mechanism improvment

An improvement to the inital retry algorithm is to modify the retry time value in each retry iteration. 
This is interesting for three things:

  1. Debounce the time we send the request to the API, so in each iteration we wait a bit more and therefore try to always find the lower value.
  1. We are currently aware of the window time the API has for the rate limit, but in case we don't know, this will help finding the best aproximate value.
  1. Add a jitter between all concurrent clients, so not all of them send the request at the same time in each iteration, which can cause a Thundering Herd Problem: https://en.wikipedia.org/wiki/Thundering_herd_problem

Note: I took as example for the algorithm the same used by KafkaJS library: https://kafka.js.org/docs/retry-detailed
